<?php

declare(strict_types=1);

class Clock implements \Pfazzi\DddCommerce\Inventory\Domain\Shared\Clock
{
    public function now(): DateTimeImmutable
    {
        return new DateTimeImmutable('new');
    }
}
