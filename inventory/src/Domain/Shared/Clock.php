<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Shared;

interface Clock
{
    public function now(): \DateTimeImmutable;
}
