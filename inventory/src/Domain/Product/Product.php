<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Product;

use Pfazzi\Ddd\AggregateRoot;
use Ramsey\Uuid\UuidInterface;

class Product
{
    use AggregateRoot;

    private UuidInterface $id;
    private Description $description;
    /** @var array<Option> */
    private array $options;

    private function __construct()
    {
    }

    /**
     * @param array<Option> $options
     */
    public static function create(
        UuidInterface $id,
        Description $description,
        array $options
    ): self {
        $product = new self();

        $product->recordAndApply(new ProductCreated($id, $description, $options));

        return $product;
    }

    public function addOption(Option $option): void
    {
        $this->assertOptionIsNotAlreadyPresent($option);

        $this->recordAndApply(new OptionAdded($option));
    }

    public function changeDescription(Description $newDescription): void
    {
        $this->recordAndApply(new DescriptionChanged($newDescription));
    }

    protected function eventAppliers(): \Traversable
    {
        yield ProductCreated::class => function (ProductCreated $event): void {
            $this->id = $event->id();
            $this->description = $event->description();
            $this->options = $event->options();
        };

        yield OptionAdded::class => function (OptionAdded $event): void {
            $this->options[] = $event->option();
        };

        yield DescriptionChanged::class => function (DescriptionChanged $event): void {
            $this->description = $event->newDescription();
        };
    }

    private function assertOptionIsNotAlreadyPresent(Option $option): void
    {
        if (\in_array($option, $this->options)) {
            throw new \InvalidArgumentException('Already present option');
        }
    }
}
