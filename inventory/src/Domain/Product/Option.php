<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Product;

class Option
{
    private string $code;

    private function __construct(string $code)
    {
        $this->code = $code;
    }

    public static function fromString(string $code): self
    {
        return new self($code);
    }
}
