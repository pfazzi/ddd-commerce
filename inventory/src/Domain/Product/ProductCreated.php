<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Product;

use Ramsey\Uuid\UuidInterface;

class ProductCreated
{
    private UuidInterface $id;
    private Description $description;
    /** @var array<Option> */
    private array $options;

    public function __construct(
        UuidInterface $id,
        Description $description,
        array $options
    ) {
        $this->id = $id;
        $this->description = $description;
        $this->options = $options;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function description(): Description
    {
        return $this->description;
    }

    public function options(): array
    {
        return $this->options;
    }
}
