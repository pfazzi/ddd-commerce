<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Product;

class OptionAdded
{
    private Option $option;

    public function __construct(Option $option)
    {
        $this->option = $option;
    }

    public function option(): Option
    {
        return $this->option;
    }
}
