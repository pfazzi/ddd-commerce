<?php

declare(strict_types=1);

namespace Pfazzi\DddCommerce\Inventory\Domain\Product;

class DescriptionChanged
{
    private Description $newDescription;

    public function __construct(Description $newDescription)
    {
        $this->newDescription = $newDescription;
    }

    public function newDescription(): Description
    {
        return $this->newDescription;
    }
}
