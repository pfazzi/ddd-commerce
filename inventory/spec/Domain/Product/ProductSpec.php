<?php

namespace spec\Pfazzi\DddCommerce\Inventory\Domain\Product;

use Pfazzi\DddCommerce\Inventory\Domain\Product\Description;
use Pfazzi\DddCommerce\Inventory\Domain\Product\DescriptionChanged;
use Pfazzi\DddCommerce\Inventory\Domain\Product\Option;
use Pfazzi\DddCommerce\Inventory\Domain\Product\OptionAdded;
use Pfazzi\DddCommerce\Inventory\Domain\Product\Product;
use Pfazzi\DddCommerce\Inventory\Domain\Product\ProductCreated;
use PhpSpec\ObjectBehavior;
use Ramsey\Uuid\UuidInterface;

class ProductSpec extends ObjectBehavior
{
    function let(
        UuidInterface $id
    ) {
        $this->beConstructedThrough(
            'create',
            [
                $id->getWrappedObject(),
                Description::fromString('StarWars T-shirt'),
                [
                    Option::fromString('size'),
                    Option::fromString('color')
                ],
            ]
        );
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Product::class);
    }

    function it_can_be_created(
        UuidInterface $id
    ) {
        $this->shouldHaveRegisteredEvent(
            new ProductCreated(
                $id->getWrappedObject(),
                Description::fromString('StarWars T-shirt'),
                [
                    Option::fromString('size'),
                    Option::fromString('color')
                ],
            )
        );
    }

    function it_allows_to_add_more_options()
    {
        $this->addOption(
            Option::fromString('with_stamp')
        );

        $this->shouldHaveRegisteredEvent(
            new OptionAdded(
                Option::fromString('with_stamp')
            )
        );
    }

    function it_throws_exception_if_option_already_exists()
    {
        $this
            ->shouldThrow(\InvalidArgumentException::class)
            ->during('addOption', [Option::fromString('color')]);
    }

    function it_allows_to_change_the_description()
    {
        $this->changeDescription(
            Description::fromString('StarWars Jedi T-shirt')
        );

        $this->shouldHaveRegisteredEvent(
            new DescriptionChanged(
                Description::fromString('StarWars Jedi T-shirt')
            )
        );
    }
}
