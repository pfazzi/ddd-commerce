<?php
declare(strict_types=1);

namespace Pfazzi\Ddd\Matcher;

use Pfazzi\Ddd\AggregateRoot;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\Matcher\BasicMatcher;

class RegisteredEventMatcher extends BasicMatcher
{
    protected function matches($subject, array $arguments): bool
    {
        /** @var AggregateRoot $subject */
        foreach ($subject->recordedEvents() as $recordedEvent) {
            if ($recordedEvent == $arguments[0]) {
                return true;
            }
        }

        return false;
    }

    protected function getFailureException(string $name, $subject, array $arguments): FailureException
    {
        return new FailureException(sprintf(
            sprintf('Expected event %s', $arguments[0]),
        ));
    }

    protected function getNegativeFailureException(string $name, $subject, array $arguments): FailureException
    {
        return new FailureException(sprintf(
            sprintf('Not expected event %s', $arguments[0]),
        ));
    }

    public function supports(string $name, $subject, array $arguments): bool
    {
        return $name === 'haveRegisteredEvent'
            && self::objectHasTrait($subject, AggregateRoot::class)
            && count($arguments) === 1
            && is_object($arguments[0]);
    }

    private static function objectHasTrait(object $subject, string $trait): bool
    {
        return in_array(
            $trait,
            array_keys((new \ReflectionClass($subject))->getTraits())
        );
    }
}
