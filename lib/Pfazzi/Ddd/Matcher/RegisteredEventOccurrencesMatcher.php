<?php
declare(strict_types=1);

namespace Pfazzi\Ddd\Matcher;

use Pfazzi\Ddd\AggregateRoot;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\Matcher\BasicMatcher;

class RegisteredEventOccurrencesMatcher extends BasicMatcher
{
    protected function matches($subject, array $arguments): bool
    {
        [$eventFqcn, $expectedOccurrences] = $arguments;

        $count = $this->countOccurrences($subject, $eventFqcn);

        return $count === $expectedOccurrences;
    }

    protected function getFailureException(string $name, $subject, array $arguments): FailureException
    {
        [$eventFqcn, $expectedOccurrences] = $arguments;

        $count = $this->countOccurrences($subject, $eventFqcn);

        return new FailureException(sprintf(
            'Expected %d occurrences of event %s, found %d.',
            $expectedOccurrences,
            $eventFqcn,
            $count
        ));
    }

    protected function getNegativeFailureException(string $name, $subject, array $arguments): FailureException
    {
        [$eventFqcn, $expectedOccurrences] = $arguments;

        return new FailureException(sprintf(
            'Not expected %d occurrences of event %s',
            $expectedOccurrences,
            $eventFqcn,
        ));
    }

    public function supports(string $name, $subject, array $arguments): bool
    {
        return $name === 'haveRegisteredEventTimes'
            && self::objectHasTrait($subject, AggregateRoot::class)
            && count($arguments) === 2
            && is_string($arguments[0])
            && is_int($arguments[1]);
    }

    private static function objectHasTrait(object $subject, string $trait): bool
    {
        return in_array(
            $trait,
            array_keys((new \ReflectionClass($subject))->getTraits())
        );
    }

    /**
     * @param $subject
     * @param $eventFqcn
     * @return int
     */
    protected function countOccurrences($subject, $eventFqcn): int
    {
        $counter = 0;

        /** @var AggregateRoot $subject */
        foreach ($subject->recordedEvents() as $recordedEvent) {
            if (get_class($recordedEvent) == $eventFqcn) {
                $counter++;
            }
        }
        return $counter;
    }
}
