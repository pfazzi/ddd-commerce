<?php

declare(strict_types=1);

namespace Pfazzi\Ddd;

use Webmozart\Assert\Assert;

trait AggregateRoot
{
    private array $events;

    public function recordedEvents(): array
    {
        return $this->events;
    }

    /**
     * @return \Traversable<string, callable>
     */
    abstract protected function eventAppliers(): \Traversable;

    private function recordAndApply(object $event): void
    {
        $this->record($event);
        $this->apply($event);
    }

    private function record(object $event): void
    {
        $this->events[] = $event;
    }

    private function apply(object $event): void
    {
        static $appliers;

        if (empty($appliers)) {
            $appliers = iterator_to_array($this->eventAppliers());
        }

        $eventFqcn = \get_class($event);

        Assert::keyExists(
            $appliers,
            $eventFqcn,
            sprintf(
                'Expected event applier for event "%s" in class "%s". Add it to "%s::eventAppliers" method.',
                $eventFqcn,
                get_class($this),
                get_class($this),
            )
        );

        $handler = \Closure::fromCallable($appliers[$eventFqcn]);

        $handler->call($this, $event);
    }
}
